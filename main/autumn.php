<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <div class = "headerbg">
        <header>TITLE</header>
    </div>

    <h3>
        <?php
            if(isset($_GET['answer']) && $_GET['answer'] == "イヤリング"){
                $ans = "正解";
            }else{
                $ans = "サンマ（秋刀魚、学名：Cololabis saira） は、ダツ目-ダツ上科-サンマ科-サンマ属に分類される、海棲硬骨魚の1種。北太平洋に広く生息する。
                食材としても重宝されて、特に日本では秋の味覚を代表する大衆魚である。";
            }
            echo $ans;
        ?>
    </h3>
    <img class="image" src="../image/enigma.png">
    <br>

    <form method="get" action="autumn.php" autocomplete="off">
        <input name="answer" type="text">
        <br>
        <input type="submit">
    </form>
</body>
</html>